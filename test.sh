#!/bin/bash

# Clean executables and stuff that we leave after testing
if [ "$1" == "clean" ]; then
    # remove test executables
    rm -rf exe/
    rm -rf input/*.s
    exit 0;
fi

logFile="log.txt"
passed=$(expr 0)
echo "Running Tests" > $logFile
for i in input/*.cm; do
    echo "Compiling $i"

    ./cmc $i 2>> $logFile

    sFile=$( echo $i | cut -d'.' -f 1,2 )".s"
    inFile=$( echo $i | cut -d'.' -f 1,2 )".in"
    outFile=$( echo $i | cut -d'.' -f 1,2 )".out"
    answerFile="input/output/"$( echo $i | cut -d'/' -f 2 | cut -d'.' -f 1,2 )".out"
    exeName=$( echo $i | cut -d'/' -f 2 )".test";
    
    echo "Result: $sFile"
    gcc $sFile -o $exeName

    echo "Testing the executable $exeName storing to $outFile"
    if [ -f $inFile ]; then
        ./$exeName < $inFile > $outFile
    else
        ./$exeName > $outFile
    fi

    result=$(diff $answerFile $outFile)
    if [ ! -z "$result" ]; then
        echo "Fail"
        echo $result
    else
        echo "Pass"
        passed=$(($passed + 1))
    fi
    echo ""
done;

mkdir -p "exe"
for i in *.test; do 
    mv $i "exe/"
done;
mv $logFile "exe/"

echo "Passed test cases: $passed"
